<?php
    $connectToSQL = mysqli_connect('localhost', 'root', 'root', 'activity2'); //Connection to sql
    if (!$connectToSQL)  //Incase of failure
    {
        die("Connection Failed with the Following Error: " . mysqli_connect_error() . "<br>");
    }
    else
    {
        $sqlcommandReadRow = "SELECT FirstName, LastName, Username, Password FROM users";  //Selection of columns
        $result = $connectToSQL->query($sqlcommandReadRow);  //Querying
        if ($result->num_rows > 0) //Checking if database table has info
        {
            while($row = $result->fetch_assoc())  //Looping while there is still info to print
            {
                echo $row["FirstName"] . " " . $row["LastName"] . " - " . $row["Username"] . " - " . $row["Password"] . "<br>";  //Printing out data
            }
        } 
        else  //Incase database table is empty 
        {
            echo "No information in database table";
        }
        $connectToSQL->close();  //Closing connection to sql
    }
?>